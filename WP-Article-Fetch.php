<?php
/*
Plugin Name: WP Article Fetch
Plugin URI: http://www.wparticlefetch.com
Description: This plugin will fetch articles and images for your blog and post them automatically at varying times for you.
Version: 2.1
Author: WP Article Fetch
Author URI: http://www.wparticlefetch.com
NOTE:  Use the last version of php (5x) and mysql (5x)
 *
 *
 *
*/

require_once('afe_ql.php');
require_once('afe_main.php');
require_once('afe_ins.php');
require_once('afe_url.php');
require_once('afe_html.php');
require_once('afe_exec.php');
ini_set('memory_limit','64M');

define('WP_ARTICLE_FETCH_URL', plugin_dir_url( __FILE__ ));
register_activation_hook(__FILE__, 'afe_activation');
register_deactivation_hook(__FILE__, 'afe_deactivation');
add_action('admin_menu', 'afe_admin_menu');
add_action('init', 'afe_initialize');
add_action('action_afe_run_campaigns','afe_run_campaigns_ajax');


 
 ?>